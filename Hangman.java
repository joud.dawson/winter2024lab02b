import java.util.Scanner;

public class Hangman {

	public static int isLetterInWord(String word, char c) {

		// Iterates through each letter of the word and returns the position of the
		// letter which the given char c is indexed.
		for (int i = 0; i < word.length(); i++) {

			if (word.charAt(i) == c) {
				return i;
			}

		}

		// Returns -1 if letter is not in word.
		return -1;
	}

	// The purpose of this function is to ensure the input is not case sensitive
	public static char toUpperCase(char c) {
		// Char function to turn the lowercase letter to uppercase.
		c = Character.toUpperCase(c);
		return c;
	}

	// Prints the current result by checking if a boolean in the array is true

	public static void printWork(String word, boolean[] letters) {
		String newWord = "";

		for (int i = 0; i < letters.length; i++) {
			if (letters[i]) {
				newWord += word.charAt(i);
			} else {
				newWord += "_";
			}
		}

		System.out.println("Your result is " + newWord);
	}

	public static void runGame(String word) {

		// Runs the game. First checks each given user letter if it is part of that
		// word.
		// If so, sets the letter numbers to 1, giving it a true value and the printwork
		// will display the letter instead of '_'. If not, the misses increases by one
		// and when misses are over 6 the player loses. Otherwise, the player wins if
		// all characters are guessed.

		boolean[] letters = { false, false, false, false };
		int numberOfGuess = 0;

		while (numberOfGuess < 6 && !(letters[0] && letters[1] && letters[2] && letters[3])) {

			Scanner reader = new Scanner(System.in);

			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess = toUpperCase(guess);

			switch (isLetterInWord(word, guess)) {
				case 0:
					letters[0] = true;
					break;
				case 1:
					letters[1] = true;

					break;
				case 2:
					letters[2] = true;

					break;
				case 3:
					letters[3] = true;
					break;
				case -1:
					numberOfGuess++;
			}

			printWork(word, letters);
		}
		if (numberOfGuess == 6) {
			System.out.println("Oops! Better luck next time :)");
		}
		if (letters[0] && letters[1] && letters[2] && letters[3]) {
			System.out.println("Congrats! You got it :)");
		}
	}

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		// Convert to upper case
		word = word.toUpperCase();

		// Start hangman game
		runGame(word);
	}
}