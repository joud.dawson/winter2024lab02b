import java.util.Scanner;

public class GameLauncher {
    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);

        System.out.println("Hello, please select a game. Type 1 or 2. \n1) Hangman\n2) Wordle");

        int chosenGame = reader.nextInt();

        if (chosenGame == 1) {
            Scanner reader2 = new Scanner(System.in);
            // Get user input
            System.out.println("Enter a 4-letter word:");
            String word = reader2.next();
            // Convert to upper case
            word = word.toUpperCase();

            // Start hangman game
            Hangman.runGame(word);
        } else if (chosenGame == 2) {
            String answer = Wordle.generateWord();
            Wordle.runGame(answer);
        } else {
            System.out.println("Invalid option, please try again.");
        }

    }
}